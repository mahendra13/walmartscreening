
// String: Manipulate strings with repeating characters aaabbbbccccc > a3b4c5

// let str1 = 'aaabbdcccccf';
let str1 = 'hhhhhqqlllllllhhhppp';
 let finalStr = str1.replace(/(.)\1*/g, function(m, $1) {
        return $1 + m.length;
    });
console.log("Manipulate strings with repeating characters:"+str1+" => ",finalStr);
