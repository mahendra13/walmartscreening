
// Replace letters in a string with the mapping below:
// [a,A] -> 4 [e,E] -> 3 [i,I] -> 1 [o,O] -> 0 [s,S] -> 5 [t,T] -> 7 [b,D] -> 5



var str = "Let's have some fun.";
// var str = "By the power of Grayskull!";
// let str = "C is for cookie, that's good enough for me";

var letters = { 
    a: '4', 
    A: '4',
    a: '4', 
    e: '3',
    E: '3',
    o: '0', 
    O: '0',
    s: '5', 
    S: '5',
    t: '7', 
    T: '7',
    b: '5', 
    B: '5'
};
let temp = [];
for (var i = 0; i < str.length; i++){
    temp.push( letters[str[i]] || str[i] );
}    
let result = temp.join('');
console.log("Replace letters in a string with the mapping : ",result);
